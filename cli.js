const NILOD = require('./nilod');
const    fs = require('fs');
const  repl = require('repl');

const NUM_SAMPLES = 50;
const PROGRESS_BAR_WIDTH = 30;
const MAX_SAMPLE_VIEW_WIDTH = 25;

let NUM_EXAMPLES = 5;

let model;

console.log(`
N o d e . j s
  I n t e r a c t i v e
\\   L i n e s
 \\    O f
  \\     D i a l o g

-------------------------------------

`);
console.log('made by Gustavo6046\n     at gugurehermann@gmail.com\n\n\n');

if (process.argv.length < 3) {
    console.log('Please supply the input filename argument.\nKeep in mind that you don\'t need to have that file in the current\ndirectory if you already have a trained NILOD model of it (\'nilod-model-<filename>.json\').');
    process.exit(1);
}

else {
    if (process.argv[2].match(/^\nilod-model-.+\.json$/))
        process.argv[2] = process.argv[2].match(/^\nilod-model-(.+)\.json$/)[1];

    if (fs.existsSync(`nilod-model-${process.argv[2].replace(/[\\/]/g, '-')}.json`)) {
        console.log('Loading model from previous training session...');

        model = NILOD.load(fs.readFileSync(`nilod-model-${process.argv[2].replace(/[\\/]/g, '-')}.json`, 'utf-8'));
        console.log('Done loading.\n');
    }

    else if (fs.existsSync(process.argv[2])) {
        model = new NILOD.NilodModel();

        console.log('Training on logs...');
        model.train(fs.readFileSync(process.argv[2], 'utf-8').split('\n'));
        console.log('Done training.\n');
        fs.writeFileSync(`nilod-model-${process.argv[2].replace(/[\\/]/g, '-')}.json`, model.save(true));
    }

    else {
        console.log('The training file was not found, and no trained model of it was found in the current directory, either. Aborting.');
        process.exit(1);
    }
}

function startGenerating(hadInput=false) {
    if (hadInput) {
        console.log('Your input will be used to select the best sample.');
        fs.writeFileSync(`nilod-model-${process.argv[2].replace(/[\\/]/g, '-')}.json`, model.save(true));
    }

    console.log('\n');
    let result = model.generate(NUM_SAMPLES, { sampleCallback: (sample, i) => {
        process.stdout.write(`\r(generating samples) [${''+Buffer.alloc(3 - (''+(i + 1)).length)}${i + 1}/${''+Buffer.alloc(3 - (''+NUM_SAMPLES).length)}${NUM_SAMPLES}] (${new Array(Math.round(i / NUM_SAMPLES * PROGRESS_BAR_WIDTH)).fill('#').join('')}${new Array(Math.round((PROGRESS_BAR_WIDTH - i / NUM_SAMPLES * PROGRESS_BAR_WIDTH))).fill(' ').join('')}) ${sample.output.slice(0, MAX_SAMPLE_VIEW_WIDTH - 3)}...${''+Buffer.alloc(PROGRESS_BAR_WIDTH + MAX_SAMPLE_VIEW_WIDTH + 3).fill(' ')}`);
    } });

    console.log(`\rScoreboard: ${''+Buffer.alloc(MAX_SAMPLE_VIEW_WIDTH + PROGRESS_BAR_WIDTH + 25).fill(' ')}\n`);
    result.scoreboard.forEach((sample, pos) => {
        console.log(`#${pos + 1}${new Array(3 - (''+pos).length).fill(' ').join('')} -- ${sample.attempt.output}`);
    });

    process.exit(0);
}

function startRating() {
    let exampleNum = 1;
    let res = null;

    while (!(res && res.success)) res = model.tryOnce();

    let state = 'GET_NUM_EXAMPLES';

    console.log('How many examples do you want to rate? The more you\nrate, the better will be the scoreboard sorting!');
    console.log(`(default: ${NUM_EXAMPLES})`);
    // NUM_EXAMPLES

    let r = repl.start({
        prompt: ': ',

        eval: (cmd, ctx, file, callback) => {
            if (state === 'RATING') {
                let output = 'Please supply a valid number.';
                let success = false;
                let rating = cmd !== '' ? +cmd : NaN;

                if (!isNaN(rating)) {
                    res.applyRating(rating / 5);
                    success = true;
                    output = `  (Rating applied | ${rating})\n`;
                }

                callback(null, {
                    text: output,
                    success: success
                });
            }

            else {
                let output = '';
                let success = true;
                let examples = +cmd;

                if (cmd === '')
                    NUM_EXAMPLES = 5;

                else if (!isNaN(examples))
                    NUM_EXAMPLES = examples;

                else {
                    output = 'Please supply a valid number.';
                    success = false;
                }

                callback(null, {
                    text: output,
                    success: success
                });
            }
        },

        writer: (output) => {
            if (state === 'RATING') {
                console.log(output.text);

                if (!output.success)
                    return;

                else if (++exampleNum > NUM_EXAMPLES) {
                    state = 'RATED';
                    startGenerating(true);
                }

                else {
                    do res = model.tryOnce();
                    while (!(res && res.success));
                    
                    console.log(`[#${exampleNum}] "${res.output}"`);
                    console.log('- Rating from -5 to 5 (decimals are allowed)');
                }
            }
            
            else if (state === 'GET_NUM_EXAMPLES') {
                console.log(output.text);

                if (output.success) {
                    if ((typeof NUM_EXAMPLES) === 'number' && !isNaN(NUM_EXAMPLES) && NUM_EXAMPLES > 0) {
                        state = 'RATING';

                        console.log(`Please rate the ${NUM_EXAMPLES} examples below, to aid the generation step:\n`);

                        do res = model.tryOnce();
                        while (!(res && res.success));
                        
                        console.log(`[#${exampleNum}] "${res.output}"`);
                        console.log('- Rating from -5 to 5 (decimals are allowed)');
                    }

                    else {
                        startGenerating(false);
                        state = 'RATED';
                    }
                }
            }
        }
    });

    r.on('exit', () => {
        startGenerating(exampleNum > 1);
    });
}

if (NUM_EXAMPLES > 0) {
    process.stdin.setEncoding('utf-8');

    startRating();
}

else
    startGenerating(false);