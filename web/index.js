const NILOD = require('nilod');
const NUM_STARS = 10;

let model = null;
// eslint-disable-next-line no-undef
let socket = io.connect();
let onRate = new Map();
// eslint-disable-next-line no-unused-vars
let historic = null;


// used in the browser console to save the current state
// of a model; all credit for this function goes to
// https://stackoverflow.com/a/30905277/5129091
function copyToClipboard(text) {
    var $temp = $('<input>');
    $('body').append($temp);
    $temp.val(text).select();
    document.execCommand('copy');
    $temp.remove();
}

socket.on('rate', (sent) => {
    onRate.get(sent.id)(model.outputHistory[sent.id].applyRating(sent.rate));
});

function mimick(sent, attemptID) {
    let resolution = {
        attemptID: attemptID,

        success: true,
        seedNode: null,
        seedText: null,
        seedQuery: null,
        output: sent.output || sent.sentence || sent,
        sentence: sent.output || sent.sentence || sent,
        outputWords: null,
        outputIDs: null,
        traversedEdges: null,
            
        reasonCode: 'EXTERNAL',
        reasonMessage: 'External sentence.',
        reasonNumber: 0,

        rating: 0,
        ratingCount: 0,

        applyRating: (rate) => {
            model.applyRating(attemptID, rate);
            resolution.rating = model.outputHistory[attemptID].rating;
            resolution.ratingCount = model.outputHistory[attemptID].ratingCount;
        },

        setRating: (rate) => {
            model.setRating(attemptID, rate);
            resolution.rating = model.outputHistory[attemptID].rating;
            resolution.ratingCount = 1;
        }
    };

    return resolution;
}

socket.on('rateme', (sent) => {
    let input = sent.input;
    let attemptID = sent.attemptID;

    let resolution = mimick(sent, attemptID);

    model.outputHistory[attemptID] = resolution;

    let genResolution = {
        sentence: sent.sentence,
        output: sent.output,
        bestOutput: sent.output,
        
        best: {
            id: sent.attemptID,
            attempt: resolution,
        }
    };

    addToContrib(input, genResolution, null);
});

// eslint-disable-next-line no-unused-vars
function addContrib(input) {
    addToContrib(null, model.generate(40), null);
}

let rated = new Map();

function rateBar(res) {
    let rating = document.createElement('div');
    rating.style.cssFloat = 'right';
    rating.style.opacity = '75%';
    rating.style.color = '#9999BB';
    let starContainer = document.createElement('span');
    let id = res.best && res.best.id || res.id || res.attemptID;

    let rate = (stars) => {
        let normalizedRating = stars * 2 / NUM_STARS - 1;
        (res.best && res.best.attempt || res).applyRating(normalizedRating);

        $.ajax({
            type: 'POST',
            url: '/api/nilod/ratesentence',
            data: JSON.stringify({id: id, rating: normalizedRating}),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            
            success: (data) => {
                rated.get(id).forEach((cb) => setTimeout(() => cb(stars, data.newRating), 0));
            }
        });
    };

    let rateCallback = (stars, newRate) => {
        let newStarContainer = document.createElement('span');
        $(rating).children().replaceWith(newStarContainer);

        function update(newRate) {
            $(newStarContainer).children().remove();

            for (let thisStar = 1; thisStar <= NUM_STARS; thisStar++) {
                let votedMe = thisStar * 2 / NUM_STARS - 1 <= stars;
                let votedAvg = thisStar * 2 / NUM_STARS - 1 <= newRate;

                let newStar;

                if (votedAvg) {
                    newStar = document.createTextNode('\u2605');
                }

                else {
                    newStar = document.createTextNode('\u2606');
                }

                let starStyler = document.createElement('span');
                starStyler.style.marginLeft = '2px';
                starStyler.style.fontSize = '17.5px';

                if (votedMe) {
                    starStyler.style.color = `rgb(${20 + (NUM_STARS - stars) * 7}, ${30 + stars * 7}, 60)`;
                }
                
                else {
                    starStyler.style.color = '#787878;';
                }

                starStyler.appendChild(newStar);
                newStarContainer.appendChild(starStyler);
            }
        }

        update(newRate);
        onRate.set(id, (newRate) => update(newRate));
    };

    if (rated.has(id)) {
        rated.get(id).push(rateCallback);
    }
    
    else {
        rated.set(id, [rateCallback]);
    }

    let isRating = false;

    for (let stars = 1; stars <= NUM_STARS; stars++) {
        let starNode = document.createElement('span');
        starNode.style.marginLeft = '2px';
        starNode.style.fontSize = '17.5px';
        starNode.style.color = `rgb(${20 + (NUM_STARS - stars) * 8}, ${30 + stars * NUM_STARS}, 60)`;

        let votedAvg = stars * 2 / NUM_STARS - 1 <= (res.best && (res.best.attempt && res.best.attempt.rating || res.best.rating) || res.rating);

        let starText = document.createTextNode(votedAvg ? '\u2605' : '\u2606');
        starNode.appendChild(starText);

        $(starNode).on('click', () => !isRating && (rate(stars), isRating = true));
        $(starNode).appendTo(starContainer);
    }
    
    rating.appendChild(starContainer);
    return rating;
}

// eslint-disable-next-line no-unused-vars
function clipboardModel() {
    copyToClipboard(model.save(true));
}

// initialization
function loadModel(data) {
    // eslint-disable-next-line no-undef
    model = NILOD.load(data);
}

function escapeHTML(text) {
    return text
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
}

let req = $.ajax({
    url: '../current.nilod.json',
    timeout: 25000
});

req.done((data) => {
    loadModel(data);

    socket.on('historic', (hist) => {
        (historic = hist).forEach((h) => {
            addToContrib(h.input, h.sent, h.id, true, {
                fontStyle: 'italic'
            });
        });
    });

    socket.emit('getHistoric');

    promptEnter(null);
});

req.fail(() => {
    let fallbackReq = $.ajax({
        url: '../default.nilod.json',
        timeout: 5000
    });

    fallbackReq.done((data) => loadModel(data));
    fallbackReq.fail(() => model = new NILOD.NilodModel());
});

// important things
// eslint-disable-next-line no-unused-vars
function promptEnter(text = null) {
    let promptInner = $('#container-promptInner');

    let sentence;
    let answer = document.createElement('p');

    answer.classList.add('answer');
    
    let waiting = document.createTextNode('...');
    answer.appendChild(waiting);

    if (text != null) {
        sentence = document.createElement('p');
        sentence.classList.add('sentence');
        sentence.innerHTML = escapeHTML('> ' + text);
        $(sentence).appendTo(promptInner);
    }

    else {
        sentence = document.createTextNode('');
        sentence.innerHTML = '&nbsp';
    }

    $(answer).appendTo(promptInner);

    function done() {
        setTimeout(() => {
            let warn = false;

            if (text != null)
                model.train([text]);

            let res;

            if (text == null)
                res = model.generate(35);
            
            else {
                res = model.generate(25, { begin: text });

                if (res == null) {
                    warn = true;
                    console.warn(`Warning: no good result generated for '${text}', resorting to random!`);
                    res = model.generate(50);
                }
            }

            answer.removeChild(waiting);
            
            answer.textContent = escapeHTML(res && (res.best && res.best.attempt || res).output || '~~');

            if (warn)
                answer.style.color = '#A08838';

            if (res != null) {
                if (typeof res === 'string')
                    answer.appendChild(rateBar(mimick(res)));

                else
                    answer.appendChild(rateBar(res));
            }

            if (res != null) {
                if (text != null) {
                    $.ajax({
                        type: 'POST',
                        url: '/api/nilod/savemodel',
                        data: JSON.stringify({sentence: text, answer: (res.best && res.best.attempt || res) || null}),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8'
                    });
                }

                else {
                    $.ajax({
                        type: 'POST',
                        url: '/api/nilod/savesentence',
                        data: JSON.stringify({sentence: (res.best && res.best.attempt || res)}),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8'
                    });
                }
            }

            setTimeout(() => {
                promptInner[0].scrollTop = promptInner[0].scrollHeight;
            }, 25);
        }, 0);
    }

    let req = $.ajax({
        url: '../current.nilod.json',
        timeout: 10000
    });
    
    req.done((data) => {
        loadModel(data);
        done();
    });

    req.fail(() => {
        done();
    });
}

function addToContrib(input, sent, id = null, addRateBar = true, style = {}) {
    let promptContrib = $('#container-rateContrib');
    let mimicked = mimick(sent, sent.attemptID || id);

    if (!model.outputHistory[sent.attemptID || id]) {
        model.outputHistory[sent.attemptID || id] = mimicked;
    }

    let answer = document.createElement('p');
    answer.classList.add('answer', 'contrib-answer');
    answer.textContent = escapeHTML(sent.output || sent);
    
    Object.assign(answer.style, style);

    if (input != null) {
        let inputEl = document.createElement('p');
        inputEl.classList.add('input', 'contrib-input');
        inputEl.textContent = '> ' + input;

        Object.assign(inputEl.style, style);

        promptContrib.append(inputEl);
    }

    if (addRateBar)
        answer.appendChild(rateBar(mimicked));

    promptContrib.append(answer);
}